/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters;

import javax.servlet.Filter;

import org.amdatu.web.filters.cachecontrol.CacheControlFilter;
import org.amdatu.web.filters.cors.CorsFilter;
import org.amdatu.web.filters.redirect.RedirectFilter;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

    private static final String CACHECONTROL_FILTER_PID = "org.amdatu.web.filters.cachecontrol";
    private static final String CORS_FILTER_PID = "org.amdatu.web.filters.cors";
    private static final String REDIRECT_FILTER_PID = "org.amdatu.web.filters.redirect";

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        manager.add(createFactoryConfigurationAdapterService(CACHECONTROL_FILTER_PID, "updated", true)
            .setInterface(Filter.class.getName(), null).setImplementation(CacheControlFilter.class)
            );

        manager.add(createFactoryConfigurationAdapterService(CORS_FILTER_PID, "updated", true)
            .setInterface(Filter.class.getName(), null)
            .setImplementation(CorsFilter.class)
            );

        manager.add(createFactoryConfigurationAdapterService(REDIRECT_FILTER_PID, "updated", true)
            .setInterface(Filter.class.getName(), null)
            .setImplementation(RedirectFilter.class)
            );
    }

}
