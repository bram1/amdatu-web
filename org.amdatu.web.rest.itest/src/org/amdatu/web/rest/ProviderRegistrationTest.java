/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;


import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_DYNAMIC;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_LEGACY;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.osgi.framework.ServiceRegistration;

/**
 * Tests the (de-)registration of providers
 */
public class ProviderRegistrationTest extends RegistrationTestBase {

    private static final String PATH = "test";
    private static final String TEST_APPLICATION_NAME = "org.amdatu.web.rest.ProviderRegistrationTestApp";
    private static final String CONTENT = "Hello World";
    private static final String PROVIDER_PREFIX = "provider: ";

    @Path(PATH)
    public static class TestResource {
        @GET
        public String handleGetRequest() {
            return CONTENT;
        }
    }

    @Provider
    public static class TestProvider implements MessageBodyWriter<String> {

        @Override
        public long getSize(String value, Class<?> clazz, Type type, Annotation[] annotations, MediaType mediaType) {
            return -1; // unknown
        }

        @Override
        public boolean isWriteable(Class<?> clazz, Type type, Annotation[] annotations, MediaType mediaType) {
            return true;
        }

        @Override
        public void writeTo(String value, Class<?> clazz, Type type, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, Object> headers, OutputStream outputStream)
            throws IOException, WebApplicationException {
            try (PrintWriter writer = new PrintWriter(outputStream)) {
                writer.write(PROVIDER_PREFIX + value);
            }
        }
    }

    public void testProviderRegistration() throws Exception {

        registerService(Application.class, new Application(), JAX_RS_APPLICATION_BASE, "", JAX_RS_APPLICATION_NAME,
            TEST_APPLICATION_NAME, JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        ServiceRegistration<Object> resourceRegistration = registerService(Object.class, new TestResource(),
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME);

        assertResponseContent(CONTENT, PATH);

        ServiceRegistration<Object> providerRegistration = registerService(Object.class, new TestProvider());

        assertResponseContent(CONTENT, PATH);

        providerRegistration.unregister();
        providerRegistration = registerService(Object.class, new TestProvider(), JAX_RS_APPLICATION_NAME,
            TEST_APPLICATION_NAME);

        assertResponseContent(PROVIDER_PREFIX.concat(CONTENT), PATH);

        providerRegistration.unregister();
        providerRegistration = registerService(Object.class, new TestProvider(), JAX_RS_APPLICATION_NAME,
            "OtherApplicationName");

        assertResponseContent(CONTENT, PATH);

        providerRegistration.unregister();
        assertResponseContent(CONTENT, PATH);

        resourceRegistration.unregister();
    }

    public void testProviderRegistrationInLegacyApplication() throws Exception {

        registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_BASE, "",
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_LEGACY);

        ServiceRegistration<Object> resourceRegistration = registerService(Object.class, new TestResource());

        assertResponseContent(CONTENT, PATH);

        ServiceRegistration<Object> providerRegistration = registerService(Object.class, new TestProvider());

        assertResponseContent(PROVIDER_PREFIX.concat(CONTENT), PATH);

        providerRegistration.unregister();
        providerRegistration = registerService(Object.class, new TestProvider(),
            JAX_RS_APPLICATION_NAME, "OtherApplicationName");

        assertResponseContent(CONTENT, PATH);

        providerRegistration.unregister();
        assertResponseContent(CONTENT, PATH);

        resourceRegistration.unregister();
    }

}
