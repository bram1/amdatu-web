/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink.util;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Variant.VariantListBuilder;
import javax.ws.rs.ext.RuntimeDelegate;

/**
 * Dummy implementation of {@link RuntimeDelegate}.
 * <p>
 * The reason for this is that JAX-RS by default delegates to this class if no other <tt>RuntimeDelegate</tt>
 * implementation could be found. Apache Wink does come with its own <tt>RuntimeDelegate</tt>, but during
 * initialization of this instance, <tt>RuntimeDelegate.getInstance()</tt> is invoked which causes a method call to
 * <tt>com.sun.ws.rs.ext.RuntimeDelegateImpl</tt> (see EntityTagMatchHeaderDelegate).
 * </p>
 * <p>
 * taken from <tt>https://amdatu.atlassian.net/wiki/display/AMDATU/OSGiification</tt>.
 * </p>
 */
public class DummyRuntimeDelegate extends RuntimeDelegate {
    @Override
    public <T> T createEndpoint(Application app, Class<T> clazz) {
        return null;
    }

    @Override
    public <T> HeaderDelegate<T> createHeaderDelegate(Class<T> clazz) {
        return null;
    }

    @Override
    public ResponseBuilder createResponseBuilder() {
        return null;
    }

    @Override
    public UriBuilder createUriBuilder() {
        return null;
    }

    @Override
    public VariantListBuilder createVariantListBuilder() {
        return null;
    }
}