/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;

import org.amdatu.web.rest.jaxrs.ApplicationService;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;

public class StaticApplicationService implements ApplicationService {

    private final Object[] m_singletons;
    private final String m_base;
    private final String m_context;
    private final String m_name;

    private volatile String m_contextPath;

    public StaticApplicationService(Object[] singletons, String base, String context) {
        this(singletons, base, context, null);
    }

    public StaticApplicationService(Object[] singletons, String base, String context, String name) {
        m_singletons = singletons;
        m_base = base;
        m_context = context;
        m_name = name;
    }

    @Override
    public String getName() {
        return m_name;
    }

    @Override
    public Object[] getSingletons() {
        return m_singletons;
    }

    @Override
    public String getBaseUri() {
        return m_base;
    }

    @Override
    public String getContextName() {
        return m_context;
    }

    @Override
    public String getContextPath() {
        return m_contextPath;
    }

    /**
     * Called by Felix DM when the ServletContextHelper service is added
     */
    protected final void onContextAdded(ServiceReference<ServletContextHelper> ref) {
        m_contextPath = (String) ref.getProperty(HTTP_WHITEBOARD_CONTEXT_PATH);
    }

    /**
     * Called by Felix DM when the ServletContextHelper service is changed
     */
    protected final void onContextChanged(ServiceReference<ServletContextHelper> ref) {
        m_contextPath = (String) ref.getProperty(HTTP_WHITEBOARD_CONTEXT_PATH);
    }

    /**
     * Called by Felix DM when the ServletContextHelper service is removed
     */
    protected final void onContextRemoved(ServiceReference<ServletContextHelper> ref) {
        m_contextPath = null;
    }

}
