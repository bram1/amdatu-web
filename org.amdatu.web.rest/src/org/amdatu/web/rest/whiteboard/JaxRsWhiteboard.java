/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_REQUEST_INTERCEPTOR;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_DYNAMIC;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_LEGACY;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_STATIC;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_ERROR;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.ws.rs.core.Application;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.rest.jaxrs.ApplicationService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.log.LogService;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

/**
 * JaxRs whiteboard is tracking
 *
 * - JAX-RS resource services with a {@link AmdatuWebRestConstants#JAX_RS_RESOURCE_BASE} service property
 * - JAX-RS application services with a {@link AmdatuWebRestConstants#JAX_RS_APPLICATION_BASE} service property
 *
 * An {@link ApplicationService} is registered for each unique base base uri.
 *
 */
public class JaxRsWhiteboard {

    private static final Object[] JAX_RS_WHITEBOARD_PROVIDERS = new Object[] { new JacksonJsonProvider() };

    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_logService;

    private final Map<ServiceReference<?>, Object> m_services;
    private final Map<ServiceReference<?>, String> m_serviceBase;

    private final Map<String, List<Component>> m_applicationServiceComponents;

    public JaxRsWhiteboard() {
        m_services = new HashMap<>();
        m_serviceBase = new HashMap<>();
        m_applicationServiceComponents = new HashMap<>();
    }

    /**
     * Handle addition of a JAX-RS application or resource service.
     *
     * @param ref the ServiceReference of the service to be added
     * @param service the service of the service to be added
     * @param baseUri the base uri of the service to be added
     */
    private void serviceAdded(ServiceReference<?> ref, Object service, String baseUri) {
        synchronized (this) {
            m_serviceBase.put(ref, baseUri);
            m_services.put(ref, service);
            updateApplicationServices(baseUri);
        }
    }

    /**
     * Handle a change in the service reference of JAX-RS application or resource service.
     *
     * @param ref the ServiceReference that has changed
     * @param baseUri the new base uri of the service that
     */
    private void serviceChanged(ServiceReference<?> ref, String baseUri) {
        synchronized (this) {
            String oldBaseUri = m_serviceBase.put(ref, baseUri);
            if (oldBaseUri != null && !baseUri.equals(oldBaseUri)) {
                // This service was previously mapped to another base uri we need to update the servlets for that old uri
                updateApplicationServices(oldBaseUri);
            }
            updateApplicationServices(baseUri);
        }
    }

    /**
     * Handle removal of a JAX-RS application or resource service.
     *
     * @param ref the ServiceReference of the service to be removed
     * @param service the service of the service to be removed
     * @param baseUri the base uri of the service to be removed
     */
    private void serviceRemoved(ServiceReference<?> ref, Object service, String baseUri) {
        synchronized (this) {
            m_services.remove(ref);
            String oldBase = m_serviceBase.remove(ref);
            if (oldBase != null) {
                updateApplicationServices(oldBase);
            }
        }
    }

    /**
     * Recalculate {@link ApplicationService} instances for a baseUri based on the currently tracked
     * JAX-RS resource and application services. Existing {@link ApplicationService} instances will be
     * unregistered and new instances will be registered.
     *
     * @param baseUri the baseUri to recalculate the ApplicationService instances for
     */
    private void updateApplicationServices(String baseUri) {
        long start = System.currentTimeMillis();

        TreeMap<ServiceReference<?>, Object> servicesForBase = getServicesForBase(baseUri);

        Map<ServiceReference<?>, Application> jaxRsApplications = new HashMap<>();
        List<Object> jaxRsResources = new ArrayList<>();

        for (Entry<ServiceReference<?>, Object> entry : servicesForBase.entrySet()) {
            if (Application.class.isAssignableFrom(entry.getValue().getClass())) {
                if (jaxRsResources.isEmpty()
                    && !containsApplicationInSameContext(jaxRsApplications.keySet(), entry.getKey())) {
                    jaxRsApplications.put(entry.getKey(), (Application) entry.getValue());
                } else {
                    Object name = entry.getKey().getProperty(JAX_RS_APPLICATION_NAME);
                    m_logService.log(LOG_WARNING, String.format(
                        "JAX-RS Application %s shadowed [base: '%s', name: '%s']", entry.getValue(), baseUri, name));
                }
            } else {
                if (!containsApplicationInDefaultContext(jaxRsApplications.keySet())) {
                    jaxRsResources.add(entry.getValue());
                } else {
                    Object name = entry.getKey().getProperty(JAX_RS_RESOURCE_NAME);
                    m_logService.log(LOG_WARNING, String.format("JAX-RS Resource %s shadowed [base: '%s', name: '%s']",
                        entry.getValue(), baseUri, name));
                }
            }
        }

        List<Component> winkServletComponents = new ArrayList<>();
        if (!jaxRsApplications.isEmpty()) {
            winkServletComponents.addAll(createApplicationServletComponents(baseUri, jaxRsApplications));
        }

        if (!jaxRsResources.isEmpty()) {
            Component component = createResourcesServletComponent(baseUri, jaxRsResources);
            winkServletComponents.add(component);
        }

        updateComponentRegistrations(baseUri, winkServletComponents);

        m_logService.log(LOG_DEBUG, "Updating servlets took: " + (System.currentTimeMillis() - start) + "ms");
    }

    private TreeMap<ServiceReference<?>, Object> getServicesForBase(String base) {
        TreeMap<ServiceReference<?>, Object> servicesForBase = new TreeMap<>(Collections.reverseOrder());
        for (Entry<ServiceReference<?>, Object> serviceEntry : m_services.entrySet()) {
            if (m_serviceBase.get(serviceEntry.getKey()).equals(base)) {
                servicesForBase.put(serviceEntry.getKey(), serviceEntry.getValue());
            }
        }
        return servicesForBase;
    }

    private boolean containsApplicationInSameContext(Collection<ServiceReference<?>> refs, ServiceReference<?> key) {
        if (isApplicationInDefaultContext(key)) {
            return containsApplicationInDefaultContext(refs);
        } else {
            return refs.stream().filter(a -> inSameContext(a, key)).findAny().isPresent();
        }
    }

    private boolean inSameContext(ServiceReference<?> ref, ServiceReference<?> other) {
        return getContextName(ref).equals(getContextName(other));
    }

    private static boolean containsApplicationInDefaultContext(Collection<ServiceReference<?>> refs) {
        return refs.stream().filter(JaxRsWhiteboard::isApplicationInDefaultContext).findAny().isPresent();
    }

    private static boolean isApplicationInDefaultContext(ServiceReference<?> ref) {
        return HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME.equals(getContextName(ref));
    }

    private List<Component> createApplicationServletComponents(String base,
        Map<ServiceReference<?>, Application> jaxRsApplications) {
        List<Component> winkServletComponents = new ArrayList<>();
        for (Entry<ServiceReference<?>, Application> applicationEntry : jaxRsApplications.entrySet()) {

            ServiceReference<?> applicationRef = applicationEntry.getKey();
            Application application = applicationEntry.getValue();

            String applicationType = (String) applicationRef.getProperty(JAX_RS_APPLICATION_TYPE);
            String applicationName = (String) applicationRef.getProperty(JAX_RS_APPLICATION_NAME);

            ApplicationService applicationService;
            String contextName = getContextName(applicationRef);
            if (applicationType == null || JAX_RS_APPLICATION_TYPE_STATIC.equals(applicationType)) {
                applicationService = new StaticApplicationService(application.getSingletons().toArray(), base,
                    contextName, applicationName);
            } else if (JAX_RS_APPLICATION_TYPE_DYNAMIC.equals(applicationType)) {
                applicationService = new DynamicApplicationService(application.getSingletons().toArray(), base,
                    contextName, applicationName, false);
            } else if (JAX_RS_APPLICATION_TYPE_LEGACY.equals(applicationType)) {
                applicationService = new DynamicApplicationService(application.getSingletons().toArray(), base,
                    contextName, applicationName, true);
            } else {
                m_logService.log(LOG_ERROR,
                    String.format(
                        "Unsupported value '%s' for application service property '%s', application will be ignored [class: '%s', service.id: '%s']",
                        applicationType, JAX_RS_APPLICATION_TYPE, application.getClass(), applicationRef.getProperty(Constants.SERVICE_ID)));
                continue;
            }

            Component component = createApplicationServiceComponent(applicationService);

            // Propagate the JAX_RS_APPLICATION_REQUEST_INTECEPTOR property to the ApplicationService component
            Object requestInterceptor = applicationRef.getProperty(JAX_RS_APPLICATION_REQUEST_INTERCEPTOR);
            if (requestInterceptor != null) {
                Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
                serviceProperties.put(JAX_RS_APPLICATION_REQUEST_INTERCEPTOR, requestInterceptor);
                component.setServiceProperties(serviceProperties);
            }

            winkServletComponents.add(component);
        }
        return winkServletComponents;
    }

    private Component createResourcesServletComponent(String base, List<Object> jaxRsResources) {
        ArrayList<Object> list = new ArrayList<>(jaxRsResources);
        Collections.addAll(list, JAX_RS_WHITEBOARD_PROVIDERS);
        ApplicationService applicationService =
            new StaticApplicationService(list.toArray(), base, HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME);
        Component component = createApplicationServiceComponent(applicationService);
        return component;
    }

    private Component createApplicationServiceComponent(ApplicationService applicationService) {
        String contextFilter = "(" + HTTP_WHITEBOARD_CONTEXT_NAME + "=" + applicationService.getContextName() + ")";
        // @formatter:off
        Component component = m_dependencyManager.createComponent()
                .setInterface(ApplicationService.class.getName(), new Properties())
                .setImplementation(applicationService)
                .add(m_dependencyManager.createServiceDependency().setService(LogService.class).setRequired(false))
                .add(m_dependencyManager.createServiceDependency().setService(ServletContextHelper.class, contextFilter)
                    .setCallbacks("onContextAdded", "onContextChanged", "onContextRemoved")
                    .setRequired(true))
                ;
        // @formatter:on
        return component;
    }

    private void updateComponentRegistrations(String base, List<Component> winkServletComponents) {
        List<Component> oldServletComponents = null;
        if (!winkServletComponents.isEmpty()) {
            oldServletComponents = m_applicationServiceComponents.put(base, winkServletComponents);
            for (Component component : winkServletComponents) {
                m_dependencyManager.add(component);
            }
        } else {
            oldServletComponents = m_applicationServiceComponents.remove(base);
        }

        if (oldServletComponents != null) {
            for (Component component : oldServletComponents) {
                m_dependencyManager.remove(component);
            }
        }
    }

    private static String getContextName(ServiceReference<?> ref) {
        String context = (String) ref.getProperty(JAX_RS_APPLICATION_CONTEXT);
        if (context == null) {
            return HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;
        } else {
            return context;
        }
    }

    /**
     * Called by Felix DM for each JAX-RS application service registration
     */
    protected final void applicationAdded(ServiceReference<Application> ref, Application application)
        throws InvalidSyntaxException {
        serviceAdded(ref, application, getApplicationBase(ref));
    }

    /**
     * Called by Felix DM for each JAX-RS application service registration change
     */
    protected final void applicationChanged(ServiceReference<Application> ref) throws InvalidSyntaxException {
        serviceChanged(ref, getApplicationBase(ref));
    }

    /**
     * Called by Felix DM for each JAX-RS application service deregistration
     */
    protected final void applicationRemoved(ServiceReference<Application> ref, Application application)
        throws InvalidSyntaxException {
        serviceRemoved(ref, application, getApplicationBase(ref));

    }

    /**
     * Called by Felix DM for each JAX-RS resource service registration change
     */
    protected final void resourceAdded(ServiceReference<Object> ref, Object resource) {
        serviceAdded(ref, resource, getResourceBase(ref));
    }

    /**
     * Called by Felix DM for each JAX-RS resource service registration
     */
    protected final void resourceChanged(ServiceReference<Object> ref) {
        serviceChanged(ref, getResourceBase(ref));
    }

    /**
     * Called by Felix DM for each JAX-RS resource service deregistration
     */
    protected final void resourceRemoved(ServiceReference<Object> ref, Object resource) {
        serviceRemoved(ref, resource, getResourceBase(ref));
    }

    private String getApplicationBase(ServiceReference<Application> ref) {
        String base = (String) ref.getProperty(JAX_RS_APPLICATION_BASE);
        return getBase(base);
    }

    private String getResourceBase(ServiceReference<Object> ref) {
        String base = (String) ref.getProperty(JAX_RS_RESOURCE_BASE);
        return getBase(base);
    }

    private String getBase(String base) {
        if (base == null || "".equals(base)) {
            return "";
        }

        if (!base.startsWith("/")) {
            base = "/".concat(base);
        }
        if (base.endsWith("/")) {
            base = base.substring(0, base.length() -1);
        }

        return base;
    }

}
