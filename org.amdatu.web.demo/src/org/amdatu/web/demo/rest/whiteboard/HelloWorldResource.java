/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.whiteboard;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.amdatu.web.demo.rest.User;
import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.Notes;
import org.amdatu.web.rest.doc.ResponseMessage;
import org.amdatu.web.rest.doc.ResponseMessages;
import org.amdatu.web.rest.doc.ReturnType;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Path("hello")
@Description("Demonstrates how to create REST endpoints.")
@Component(provides=Object.class, properties = {
    @Property(name = JAX_RS_RESOURCE_BASE, value = "/rest")
})
public class HelloWorldResource {
    private volatile User m_lastUser = new User("John", "Doe");
    @Context
    private HttpServletRequest request;

    /**
     * @return a plain text string, when sending a GET-request with the header "Accept: text/plain".
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Description("Will return a friendly message from your server.")
    @ResponseMessages({ @ResponseMessage(code = 200, message = "Friendly text message") })
    public String helloPlain() {
        return "hello world";
    }

    @GET
    @Path("response-with-headers")
    @Produces({ MediaType.APPLICATION_JSON })
    @Description("Will return a friendly message from your server with headers.")
    @ResponseMessages({ @ResponseMessage(code = 200, message = "Friendly JSON message with extra header") })
    @ReturnType(String.class)
    public Response helloResponse() {
        return Response.ok("\"hello world\"").header("Link", "http://www.amdatu.org/").build();
    }

    /**
     * @return a JSON text string, when sending a GET-request with the header "Accept: application/json" or "Accept: application/vnd.custom+json".
     */
    @GET
    @Path("custom-mediatype")
    @Produces({ MediaType.APPLICATION_JSON, "application/vnd.custom+json" })
    @Description("Will return a friendly message from your server.")
    @ResponseMessages({ @ResponseMessage(code = 200, message = "Friendly JSON message") })
    public String helloJSON() {
        return "\"hello world\"";
    }

    /**
     * @return a plain text string, when sending a GET-request with the header "Accept: text/plain".
     */
    @GET
    @Path("with-uri-info")
    @Produces("text/plain")
    @Description("Returns a message with some context information")
    public String hello2(@Context UriInfo uriInfo, @Context HttpServletRequest request) {
        return "Hello world from " + uriInfo.getAbsolutePath();
    }

    /**
     * @return a JSON text string denoting a {@link User}, when sending a GET-request with the header "Accept: application/json".
     */
    @GET
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Returns the first and last name of the user.")
    public User user() {
        return m_lastUser;
    }

    /**
     * @param user the new user information, when sending a POST-request with the header "Content-Type: application/json" or "Content-Type: application/vnd.custom+json".
     */
    @POST
    @Path("user")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Description("Sets the first and last name for the user, needs a FORM object with the fields 'first' and 'last'.")
    @Notes("The order of the fields is 'first', 'last'.")
    @Deprecated()
    public void setUser(@FormParam("first") @DefaultValue("John") String firstName, @FormParam("last") @DefaultValue("Doe") String lastName) {
        m_lastUser = new User(firstName, lastName);
    }

    /**
     * @param user the new user information, when sending a POST-request with the header "Content-Type: application/json" or "Content-Type: application/vnd.custom+json".
     */
    @POST
    @Path("user")
    @Consumes({ MediaType.APPLICATION_JSON, "application/vnd.custom+json" })
    @Description("Sets the first and last name for the user, needs a JSON object with the fields 'first' and 'last'.")
    @Notes("The order of the fields is irrelevant.")
    @ResponseMessages({ @ResponseMessage(code = 415, message = "In case of an unknown body.") })
    public void setUserViaJSON(User user) {
        m_lastUser = user;
    }

    /**
     * @return a JSON text string denoting a {@link User}, when sending a GET-request with the header "Accept: application/json".
     */
    @GET
    @Path("user/{message}")
    @Produces(MediaType.TEXT_PLAIN)
    @Description("Greets the user with a message.")
    public String greetUser(@PathParam("message") @DefaultValue("Hello") String message) {
        return String.format("%s %s %s", message, m_lastUser.getFirst(), m_lastUser.getLast());
    }

    @POST
    @Path("post-array")
    @Produces({ MediaType.APPLICATION_JSON })
    @Description("Will save an array of users.")
    public void saveArray(User[] users) {

    }

    @POST
    @Path("post-list")
    @Produces({ MediaType.APPLICATION_JSON })
    @Description("Will save a list of users.")
    public void saveList(List<User> users) {

    }

    @GET
    @Path("users-array")
    @Description("Get an array of users.")
    public User[] getUserArray() {
        return new User[] {};
    }

    @GET
    @Path("users-list")
    @Description("Get a list of users.")
    public List<User> getUserList() {
        return Collections.emptyList();
    }
}
