/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.whiteboard;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Component(provides = Application.class, properties = {
    @Property(name = JAX_RS_APPLICATION_BASE, value = "/app")
})
public class WhiteboardRestApplication extends Application {

    private final Set<Object> m_singletons;

    public WhiteboardRestApplication() {
        m_singletons = Collections.singleton(new WhiteboardApplicationResource());
    }

    @Override
    public Set<Object> getSingletons() {
        return m_singletons;
    }

}
